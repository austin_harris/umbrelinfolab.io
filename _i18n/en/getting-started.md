Getting started with Umbrel

If you are a new user of Umbrel, here you have a mini guide and links about how to use this amazing simple software node. It is important that if you start from zero knowledge, to begin with reading about what is Lightning Network and how to use it, basic concepts and functionalities. In this guide you will find many links to those resources. Do NOT ignore them, read them and then put them in practice with your Umbrel node.

Table of contents
{: .no_toc .text-delta }
1. TOC
{:toc}

## **Important links**

**As a general note:** if you are new into this area of LN (Lightning Network), please educate yourself, at least the basics of LN, BEFORE starting the Umbrel node.

| Main Links | Reading Links |
| ------ | ------ |
| [Umbrel Info Page](https://info.umbrel.tech/) (this page)  | [Lightning Network Resources](https://www.lopp.net/lightning-information.html) – by Jameson Lopp |
| [Get Umbrel Page](https://getumbrel.com/) | [Lightning Node Management](https://openoms.gitbook.io/lightning-node-management/) – by openoms |
| [Umbrel Community Forum](https://community.getumbrel.com/) | [Beginners LN Guide](https://bitcoiner.guide/lightning/) – by Bitcoiner Guide |
| [Umbrel Community Telegram Group](https://t.me/getumbrel) | [LightningWiki](https://lightningwiki.net/) – by BitcoinStickers |
| [Umbrel Community Discord Server](https://discord.gg/VY3SsPZZya) | [Wiki Lightning Network](https://wiki.ion.radar.tech/) – by ION Radar Tech |
| [Umbrel Github Page](https://github.com/getumbrel/umbrel) | [What is Lightning Network](https://academy.binance.com/en/articles/what-is-lightning-network) – by Binance Academy |
|[Alex Bosworth Talks Routing Nodes](https://lightningjunkies.net/lnj036-alex-bosworth-talks-routing-nodes/) – by Lightning Junkies | [An Overview of Lightning Network Implementations](https://medium.com/@fulgur.ventures/an-overview-of-lightning-network-implementations-d670255a6cfa) |
| [Video tutorials about LN](https://www.youtube.com/c/MinistryofNodes/videos) – by Ministry of nodes |	[Node Doctor](https://terminal.lightning.engineering/) – Checking your node and peers suggestion |
| [Video tutorials about LN](https://www.youtube.com/user/renepickhardt/videos) – by René Pickhardt | [ln.bigsun.xyz](https://ln.bigsun.xyz/) – LN nodes explorer and statistics |
| [LN channels mangement](https://youtu.be/HlPIB6jt6ww) – by Alex Bosworth | [1ml.com](https://1ml.com/) / [Amboss](http://amboss.space) – LN explorere |

## **Installation modes**

First and foremost: default password of Umbrel node is **moneyprintergobrrr**

Second: EACH Umbrel app, will have a different onion address, so if you plan to use it outside your LAN, save those onion addresses, but NEVER expose/share them in public.

Third: change the default password of you Umbrel node, but keep in mind that for the moment Thunderhub and Lightning Terminal app will not update that password and will still have the default password. You can change that password, digging up manually into the belly of each app repository.

Fourth: **WAIT** for the node to fully synced and don't send any funds to your node wallet until then. You can't use them anyway until is synced, so why so much rush? **Patience** is the key for a node operator.

### **Using a Raspberry Pi 4**

- [Recommended hardware parts](https://community.getumbrel.com/t/recommend-hardware-links-for-different-countries/33) using RaspPi for Umbrel node, we recommend those parts. many users had issues using other parts.
- [Video tutorial](https://youtu.be/fppmhqjqh2E) (by BTC Sessions) about install/setup an Umbrel node from zero, using a Raspberry Pi
- [Github Umbrel](https://github.com/getumbrel/umbrel-os) install instructions


### **Using a normal PC/laptop/NUC**

- Recomended min. hardware configuration: x64 CPU, 8GB RAM memory, 1TB HDD (optimal SSD). One example here: [Gigabyte NUC](https://imgur.com/a/twdZANs)
- OS software base: [Debian](https://www.debian.org/) or [Ubuntu](https://ubuntu.com/download/desktop), or any other Linux distro you are comfortable with
- [Github Umbrel install instructions for Linux](https://github.com/getumbrel/umbrel#-installation) – just follow the simple instruction and in 5 min you have it installed
- [Video turorial install Umbrel on Ubuntu](https://www.youtube.com/watch?v=EJuUVKuEwNQ)

**Step by step commands:**

Preliminary step - update system OS
Open Terminal command window and first thing let's update the whole system with:

`sudo apt-get update` & `sudo apt-get upgrade`

(restart if is necessary)

Preliminary step - set static IP to your linux machine
[How to configure static IP in Ubuntu GUI](https://youtu.be/kPLPJK5SzGg)

**1 - Install docker**

Follow all the instructions from [Docker install page](https://docs.docker.com/engine/install/ubuntu/)

**2 - Install Python** - check first if there's a version installed with

`python3 ––version`

then if there's no python installed or too old next step

`sudo apt install python3.9.6`

**3 - Install Docker Compose**

`sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`

then

`sudo chmod +x /usr/local/bin/docker-compose`

If you have problem to install see: [alternative install](https://docs.docker.com/compose/install/#alternative-install-options) or just run

`sudo pip install docker-compose`

then

`sudo chmod +x /usr/local/bin/docker-compose`

**4 - Install aditional requisites**

`sudo apt-get install fswatch jq rsync curl`

**5 - Install Umbrel**

Create a folder for Umbrel repository:

`md Umbrel` & `cd Umbrel`

`curl -L https://github.com/getumbrel/umbrel/archive/v0.3.14.tar.gz | tar -xz --strip-components=1`

DONE, Umbrel package is deployed.

Start Umbrel

`sudo ./scripts/start`

It will start extracting the packages and install them in the Umbrel folder.

As you will see the message at the end of the starting script, you can access your node at [http://umbrel.local](http://umbrel.local) or http://192.x.x.x (your node's local IP). Just use same linux machine to access it or any other PC from your LAN to access that node page dashboard.

You will be asked to choose a user name, save the 24 words seed and set a password. Please write down all those and save them in a secure place.

Once is installed and running, it start syncing the Bitcoin Blockchain. In this moment you just relax and wait to be synced. It will take 4-12 days, depending on the hardware and internet connection. Meanwhile I invite you to learn more about Lightning Network and how to use and be preapred to use an Umbrel node.

## **General Rules**

### **Channel size**

When you open channels, **DO NOT** start with small amount like 20-50-100k sats. That ridiculous low amount will not be enough not even for the open/close fees. Low amount channels are doing more harm than good, to you and for the rest of the network.

Example: If you have a 20k channel open with my node:

- that will barely cover the open/close fees and remains only dust to spend.
- If I want to use that channel to send 50k sats it will not be possible. So the channel will be useless and will lower the node score for both nodes.

Open channels with min 500k-1M sats. That will offer a better routing, for you and for all others that will route txs through your node.

“Bigger is better” DO NOT apply too much in this case, so now don’t go to the other extreme and open massive 0.5BTC channels. Much better the approach of having 5-6 outbound channels with each between 500k to 1M sats and also, depending of your needs, 3-4-5 inbound channels, with the same amount.

Now, yes, more channels is better, because your node will be better connected and will find a better route and faster.

**OUTBOUND Liquidity** – You have a LN node to make LN payments, buying stuff, sending to friends, paying services etc. So try to open LN channels with those merchants you are willing to trade (merchants, exchanges, wallet operators, LN services, friends with nodes).

**INBOUND liquidity** – So find some peers that are willing to open some channels TOWARDS your node. Check the Umbrel Discord server and Telegram group, where you can ask for peers to open channels with your node. The inbound liquidity IT IS NECESSARY in order to be able to receive LN payments.

**What/with who should I open channels?**
- First, with those merchants/shops that you will do txs, you will buy from them, the txs will have zero fees and you have direct routing.
- Second, with friends/LN freaks you know and can create a ring of nodes with specific amount os sats for channels, balancing the funds and lowering the fees between nodes in the ring.
- Third, your ring of nodes can have some "external" connections/channels with other good nodes (ideal that each member of the ring with a different node) so you can route easily and fast anywhere.

### **Node Operator**

Operating a node, doesn’t mean automatically “I will get rich earning sats“. Move away from this wrong mentality.

A. You run the Umbrel node to protect your privacy, to protect your keys, to protect your money, to protect the custody of your wallets.

B. Yes, you will earn some sats, but only if you will manage your node in the right way. But even then, those earnings will be insignificant to be considered “earnings”.

C. You run this node to LEARN more about Bitcoin and find solutions for real life applications. Umbrel is offering that opportunity with many apps included (see LNbits, BTCPay, LN Pool, Whirlpool) ready for use cases.

D. **DO NOT TURN OFF YOUR NODE!** I've seen people turning off their node at night (they complain that can't sleep). Why have you run it if you don't have it ON 24/7/365? A node has to be ONLINE as long is possible. It is also advisable to put a UPS and avoid power outages. This is your Bank and also others depend on it! 


## **Basic node usage**

Now, what I suggest, for any new node user, in special with Umbrel:

### **Wait to sync the blockchain**
It will take time, I know, but be patient. It will get through, no worry. Don’t do anything (stupid) until is not ready. Depending on your hardware configuration and internet speed, it could take between 3 and 12 days. After the sync, it will take another 8-12h to sync the index for Electrum Server.

### **Keep your software updated**

Not just Umbrel, if you are not using the Pi mode install. So once is synced, check if there are new updates and apply them. 
If you are using RaspPi option, updating Umbrel will update also the OS. 
If you are using a Linux machine with Umbrel in docker, I would recommend to do the following procedure (in Terminal):
- Stop Umbrel node: `sudo ./scripts/stop`
- Save your lnd.conf file if you edited (add color, name or specific features to your node), see in home/umbrel/lnd
- Update system: `sudo apt update` and then `sudo apt upgrade`
- Restart system
- Update Umbrel: `cd ~/umbrel && sudo ./scripts/update/update --repo getumbrel/umbrel#v0.3.9` (replace version with the latest release)
- Replace lnd.conf file with the one you save it before update
- Start Umbrel: `sudo ./scripts/start`
- Leave the node to catch up with blocks and logs, usually takes several minutes, be patient. Now you can enter into dashboard Umbrel

### **Install minimum required apps like**

See the Umbrel Apps section:
- Ride The Lightning (RTL) – management for node, wallet, channels, routing.
- ThunderHub – management for node, wallet, channels, routing, chat, tools.
- BTC RPC Explorer – Blockchain explorer and tools
- Mempool – watch mempool fees, blocks, check txs, blockchain explorer, very handy

### **Install optional apps**

See the Umbrel section of App Store, one click install:

- [LNBits Suite](https://info.umbrel.tech/guides.html#umbrel--lnbits) – amazing suite of tools with LNURLpay/withdraw, LN TPoS, LNDHUB and many more.
- [LNDHub BlueWallet](https://info.umbrel.tech/guides.html#umbrel--lndhub--bluewallet) - Be the bank for your family and friends that do not have a node. Amazing way for having own custody of your funds with a node.
- [BTCPay Server](https://btcpayserver.org/) – very good backend for webshops/shops, donations, POS, business tool, [YT channel here](https://www.youtube.com/c/BTCPayServer/featured)
- Samourai Whirlpool – privacy and mixing tool, connection for your Samourai wallet. [A great detailed guide here](https://bitcoiner.guide/) and an interview with the [developer of Samourai wallet here](https://stephanlivera.com/episode/150/)
- [Sphinx Chat](https://sphinx.chat/) – very interesting tool for podcasters, private groups chat using LN for tips and chats. Be aware! In order to NOT be charged a fee EVERY time you login or change profile, you will have to [open a 100k sats channel with Sphinx app node](https://github.com/stakwork/sphinx-relay/wiki/Home-node-FAQ)

### **Connect some wallets to your node**

A. See the "Connect Wallet" in your Umbrel dashboard page that has easy-to-understand instructions for connecting many wallets:
- Zap, Zeus - to manage your node remotely, using onion connection (desktop and mobile version)
- Electrum - to verify/broadcast your txs from your wallet using your own node, also using it as swap funds from/in to your node
- Specter - connect to your node and use your favorite hardware wallets in a secure way
- And many more...

B. [Bluewallet + LNDHub BW](https://info.umbrel.tech/guides.html#umbrel--lndhub--bluewallet) - to offer hosted wallets in your node to your family and friends. This is a wallet management not node management!

C. [BlueWallet/Zap + LNbits](https://info.umbrel.tech/guides.html#umbrel--lnbits) - a nice way to have your own lndhub wallets and extensions for vary usages.

### **Connect your onchain node wallet with [Bluewallet](https://bluewallet.io/)**

Yes, is true, now you can connect directly to a mobile app, the onchain AEZEED wallet. Just use your Umbrel node seed in Bluewallet (adding it as new wallet) and done. Use cases:

- you want to have at hand a way to deposit quick to your node wallet (onchain)
- you need to have access to your onchain funds in case your node is crashed and want to recover / access the funds.

## **Funding your node**

- Keep in mind: your Umbrel onchain wallet should act as a bridge/ramp between your many other wallets or BTC sources and your LN channels. This wallet is used to fund the opening and closing channels on your LN node. So in order to be able to open LN channels you will need to send some funds into this onchain wallet.

- Go to your Umbrel main interface, Bitcoin wallet and click “receive”. From any other wallet send some funds (usually for 3-4 channels, each 1M sats). Wait to be confirmed then go to next step.

- You can use Lightning wallet, from the same main Umbrel interface, but I suggest to use Thunderhub app. Why? Because you can control better the miner fees for opening the channel. Also RTL app is good, but still you can’t see how much fee in total you are paying. So use one of these 3 interfaces and go to open channels. Select a node desired and put at least 500k-1M sats and open a channel. Start with one good node (see [https://ln.bigsun.xyz](https://ln.bigsun.xyz) or [https://1ml.com](https://1ml.com) ) which node has longest history or number of channels) and later continue with other users nodes. Help new users that post their new nodes URI in the Umbrel Community Telegram group and ask for new channels. TOGETHER STRONGER.

- Once you open a LN channel, until is not confirmed (usualy takes 3 confirmations) the UTXO from where you used the sats to open the channel will not be usable until is not confirmed the new UTXO. [See here what means UTXO](https://learnmeabitcoin.com/technical/utxo). In other words, until is not confirmed, you cannot use those remaining sats for opening a new channel. Wait. Be patient, don't freak out that your funds are not reflected in total balance (yet). Optional you can go to RTL app and check the UTXOs in the onchain wallet.

- Balance your LN channels. You can use the Umbrel app Thunderhub to open and balance your channels with the peers. This will provide a better routing for your txs and also for other users txs that are routing through your node. [Here you have a simple guide using Thunderhub](https://apotdevin.com/blog/thunderhub-balancing). Another option could be: open a channel towards a peer, let's say 1M sats. Then once is open, go and make a payment to an external wallet (for example Bluewallet) with half of the balance. In this way you are balancing the channel for in and out txs. Optional, you can loop out from the same channel, back to your onchain node wallet and re-use that amount to open another channel with another peer.

- Are you going to do some shopping with LN? Good, then go and open channels with those merchants you are planning to buy from. In this way, you get to pay less fees, your txs will get through faster, you help also the merchants to have better inbound. Optional you can ask them to lower the fees for your channel. Even if you are not planning to do that shopping with them, doesn’t matter, just open the channels. It will be used also for routing, if there are with a good amount.

- On [Umbrel Discord server](https://discord.gg/VY3SsPZZya) you can also find a room where new Umbrel users can exchange info to open mutual channels. That will help a lot the new users, to start with some known node operators and with some liquidity.

- **Video Tutorials about how to open/fund LN channels:**

    - [Video tutorial how to build own Umbrel node](https://youtu.be/fppmhqjqh2E) – by BTC Sessions
    - [Video tutorial how to run Umbrel + ThunderHub](https://www.youtube.com/watch?v=KItleddMYFU) - by BTC Sessions
    - [Video Tutorial RTL app](https://youtu.be/DfRYJcBsfkA?t=155) – by Ministry of Nodes
    - [Video Turorial RTL rebalancing channels](https://www.youtube.com/watch?v=09tLEZM4MgQ)
    - [Video Tutorial ThunderHub app](https://www.youtube.com/watch?v=hZ3ttYFliL4) – by ThunderHub
    - [LN channels mangement](https://youtu.be/HlPIB6jt6ww) – by Alex Bosworth

- **Lightning Network Ring of Fire** - Interesting group to create a ring of nodes, opening channels to each others. [Here is a better explanation of how this works](https://github.com/Czino/ring-of-fire). If you want to join their Telegram group and participate in different types of rings, [here is the link](https://t.me/joinchat/Uao0Z_hBequXkeB0)

- A very good thread about [channel re-balancing here](https://community.getumbrel.com/t/to-read-do-all-lightning-channels-need-to-be-balanced/817), by Czino (Rings of Fire)

- Wait for the opening channel to be confirmed (usually 3 confirmations) and done, you are ready to start lightning, using the wallets I indicated before.

## **New features needed Umbrel node**

* I would like to see future Umbrel releases that can open channels using all onchain wallet balance, deducting automatically the fee. Guessing the correct fee always end with some dust sats remaining in the onchain wallet. That's not good.
* Streaming services. Would be nice to have a streaming service, as a separate app. Maybe as pro/paid feature, so the devs can receive something?
* BTCPay app – option to switch off Tor or add option to use Tor/clearnet for an external webshop. Right now BTCPay instance from Umbrel can be used ONLY with .onion webshops. We don’t expect that girls will buy cosmetics and shoes using Tor…
* If you have new ideas about a new feature, you can post on [Umbrel Github page](https://github.com/getumbrel/umbrel/issues), mentioning in title that is a "Feature request" and explaining in details how do you think it should be/work. If there are interesting and useful proposals, devs will discuss them and take in consideration for future development.

## **In closing**

I hope that this mini-guide will help you as a noob to start using your node. Slowly educate yourself and find your own best way to use your node.
There are many other things to do with a node, also many other apps and usages, but this is just the beginning.

**HAPPY LIGHTNING!**

---

This guide was created by [DarthCoin](https://bitcoinhackers.org/@DarthCoin). If you want to test your new Umbrel node by tipping DarthCoin, you can generate a LNUrl [here](https://lntxbot.bigsun.xyz/@DarthCoin). The LNUrl can be read by Zeus Wallet Zap (Android mobile) or Thunderhub app.
