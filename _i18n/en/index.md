## Additional resources about Umbrel

This page features additional information about Umbrel software node that can't be found on the [official website](https://getumbrel.com).
This page is intended to answer your questions about Umbrel and help you with more information about hoow to use this software.
This is open source software and the support is done by [Umbrel Community users](https://community.getumbrel.com/)

This page is not an official Umbrel page.
In addition, this page only applies to the latest version of Umbrel (currently v0.3.10).

This page is avaiable in multiple langauges: [German](/de/) - [Español](/es/) - [French](/fr/)

---

## Structure

1. Introduction (this page)
2. [Getting started](getting-started.html): Getting started with Umbrel.
3. [Umbrel Apps Guides](guides.html): How to use specific apps within Umbrel.
3. [Troubleshooting](troubleshooting.html): Issues concerning the primary operations of your Umbrel node.
4. [General FAQ](faq.html): General questions and further learnings regarding the technology used in Umbrel.

---

## A word of caution

Umbrel is still in beta and should not be considered secure.
We do not recommend running it with much money yet to avoid losing money.

---

This page is part of [Umbrel Labs](https://umbrel.tech), a free service extending your Umbrel node.

---
