# Frequently Asked Questions
{: .no_toc }

General questions and further learnings regarding the technology used in Umbrel.

Issues concerning the primary operations of your Umbrel node can be found in the separate [Troubleshooting](troubleshooting.md) guide.

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

### Can I get rich by routing Lightning payments?

Nobody knows. Probably not. You will get minimal fees. I don't care. Enjoy the ride!

### Can I attach the Ext4 formatted hard disk to my Windows computer?

The Ext4 file system is not compatible with standard Windows, but with additional software like [Linux File Systems](https://www.paragon-software.com/home/linuxfs-windows/#faq) by Paragon Software (they offer a 10 days free trial) it is possible.

### Can I use previously downloaded blockchain to save time when setting up my Umbrel node?

This is not natively supported by Umbrel for different reasons but you can make it at your own risk following these instructions:

1. Flash Umbrel OS to an SD card and boot it in a Pi with your SSD attached
2. Run through the setup process
3. Go to the settings page and shutdown Umbrel (Your SSD is now formatted correctly for Umbrel)
4. Remove the SSD and attach it to a host Linux machine with access to the pre-downloaded blockchain
5. Copy blocks and chainstate directories from your Bitcoin Core data directory to /umbrel/bitcoin/ on the SSD
6. Remove the SSD from the host, plug it back into your Umbrel, and power it back on.

### What do all the Linux commands do?

This is a (very) short list of common Linux commands for your reference. For a specific command, you can enter `man [command]` to display the manual page (type `q` to exit).

| command      | description                        | example                                      |
| ------------ | ---------------------------------- | -------------------------------------------- |
| `cd`         | change to directory                | `cd /home/umbrel`                            |
| `ls`         | list directory content             | `ls -la /home/umbrel/umbrel`                 |
| `cp`         | copy                               | `cp file.txt newfile.txt`                    |
| `mv`         | move                               | `mv file.txt moved_file.txt`                 |
| `rm`         | remove                             | `rm temporaryfile.txt`                       |
| `mkdir`      | make directory                     | `mkdir /home/umbrel/newdirectory`            |
| `ln`         | make link                          | `ln -s /target_directory /link`              |
| `sudo`       | run command as superuser           | `sudo nano textfile.txt`                     |
| `su`         | switch to different user account   | `sudo su root`                               |
| `chown`      | change file owner                  | `chown umbrel:umbrel myfile.txt`             |
| `chmod`      | change file permissions            | `chmod +x executable.script`                 |
| `nano`       | text file editor                   | `nano textfile.txt`                          |
| `tar`        | archive tool                       | `tar -cvf archive.tar file1.txt file2.txt`   |
| `exit`       | exit current user session          | `exit`                                       |
| `systemctl`  | control systemd service            | `sudo systemctl start umbrel-startup`        |
| `journalctl` | query systemd journal              | `sudo journalctl -u umbrel-external-storage` |
| `htop`       | monitor processes & resource usage | `htop`                                       |
| `shutdown`   | shutdown or restart Pi             | `sudo shutdown -r now`                       |

### How can I see the logs of a specific Umbrel module?

Step 1 - In a terminal window, execute this:

```ssh -t umbrel@umbrel.local```

Password: < your own Umbrel dashboard password > (remember that you will not see what you type, then press ENTER)

Step 2 - type this command, replacing the name of each docker container you want to see (tor, lnd, bitcoin, btcpay, lnbits etc)

```docker-compose logs tor```

### Where can I get more information?

If you want to learn more about Bitcoin and are curious about the inner workings of the Lightning Network, the following list of articles and tutorials can offer a very good introduction:

General Info about Bitcoin and Lightning Network:

- [What is Bitcoin?](https://bitcoinmagazine.com/guides/what-bitcoin)
- [Understanding the Lightning Network](https://bitcoinmagazine.com/articles/understanding-the-lightning-network-part-building-a-bidirectional-payment-channel-1464710791/)
- [Bitcoin resources](https://www.lopp.net/bitcoin-information.html) and [Lightning Network resources](https://www.lopp.net/lightning-information.html) by Jameson Lopp
- [Lightning Network Node Management](https://openoms.gitbook.io/lightning-node-management/) - by openoms
- [Beginners LN Guide](https://bitcoiner.guide/lightning/) - by Bitcoiner Guide
- [Wiki Lightning Network](https://wiki.ion.radar.tech/) - by ION Radar
- [An overview of LN implementations](https://medium.com/@fulgur.ventures/an-overview-of-lightning-network-implementations-d670255a6cfa)
- [Tor-Only Bitcoin and LN nodes](https://blog.lopp.net/tor-only-bitcoin-lightning-guide/) - by Jameson Lopp
- [What is a Bitcoin Node ?](https://youtu.be/sVeolsQ3cvU)
- [Lightning Network Explained](https://www.youtube.com/watch?v=rrr_zPmEiME) - by Simply Explained
- [How LN channels work](https://www.youtube.com/watch?v=pOZaLbUUZUs) - by Decentralized thought
- [LN Channels Management](https://youtu.be/HlPIB6jt6ww) - by Alex Bosworth
- [Video tutorials about LN](https://www.youtube.com/user/renepickhardt/videos) - by René Pickhardt
- [Path finding in LN](https://www.youtube.com/watch?v=MeEFUaRnMak) - by René Pickhardt

Umbrel Tutorials:

- [How to build your own Umbrel with RaspPi](https://youtu.be/fppmhqjqh2E) - by BTC Sessions
- [How to install Umbrel on Ubuntu](https://www.youtube.com/watch?v=EJuUVKuEwNQ)
- [How to use Umbrel with Thunderhub](https://www.youtube.com/watch?v=KItleddMYFU) - by BTC Sessions

Lightning Network Apps:

- [LNBits Tutorials Channel](https://www.youtube.com/channel/UCGXU2Ae5x5K-5aKdmKqoLYg/videos) - by Ben Arc
- [BTCPay Video Tutorials Channel](https://www.youtube.com/c/BTCPayServer/featured) - by BTCPay Server
- [How to use RTL app/wallet](https://youtu.be/DfRYJcBsfkA?t=155) - by Ministry of Nodes
- [Rebalancing channels with RTL](https://www.youtube.com/watch?v=09tLEZM4MgQ)
- [How to Loop out from a LN channel with RT](https://www.youtube.com/watch?v=_rK4gYaEybE)
- [How to use ThunderHub app/wallet](https://www.youtube.com/watch?v=hZ3ttYFliL4)
- [Channels Management with RTL](https://www.youtube.com/watch?v=pESO_Pm0v10)

Video Tutorials for wallets

- [How to connect my desktop wallets to a node](https://www.youtube.com/watch?v=pyylkpR4DDk) - by BTC Sessions
- [Specter Desktop Tutorial](https://youtu.be/xHIzQv8FTFs) - by Ben Kaufman
- [HWI Bridge Configuration with Specter](https://www.youtube.com/watch?v=rUOxjyOGOGw)
- [Lightning Network wallets](https://www.youtube.com/watch?v=lcBsn8e-oQ4&list=PLxdf8G0kzsUWcgEJLH9AHTN3KQzoN2HTs) - playlist by BTC Sessions
- [Bitcoin Mobile wallets](https://www.youtube.com/watch?v=imMX7i4qpmg&list=PLxdf8G0kzsUUE7HHNTGTWBFxzt2oudiyS) - playlist by BTC Sessions
- [Bitcoin Desktop wallets](https://www.youtube.com/watch?v=ECQHAzSckK0&list=PLxdf8G0kzsUVzpmuXsCjtm3XWblNzdeEL) - playlist by BTC Sessions
- [How to use Hardware wallets](https://www.youtube.com/watch?v=0Zg6hKChKp0&list=PLxdf8G0kzsUVkZ5_Jc6PyG_j4K8htg-i3) - playlist by BTC Sessions
- [How to use Multisig wallets](https://www.youtube.com/watch?v=ZQvCncdFMPo&list=PLxdf8G0kzsUUqr4oVXRHL1L-iK1q9hCfq) - playlist by BTC Sessions

Video Tutorials for privacy using nodes

- [How to use Whirlpool and Samourai with your node](https://www.youtube.com/watch?v=M3g5ApifWAY) - by Ministry of Nodes
- [Whirlpool and mixing guide](https://bitcoiner.guide/whirlpool/) - by Bitcoiner Guide

### Does Umbrel support …?

Currently not, but Umbrel has an application infrastructure, so third-party developers can add apps to Umbrel and publish them in its [App Store](https://medium.com/getumbrel/introducing-the-umbrel-app-store-7a2068c64a10).

---

This General FAQ guide will be constantly updated with findings that have been or will be reported in the issues section. Feel free to contribute via a merge request.

---
