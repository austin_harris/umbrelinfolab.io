Empezando con Umbrel

Si eres un nuevo usuario de Umbrel, aquí tienes una mini guía y enlaces sobre cómo usar este increíble y sencillo software para un nodo Bitcoin y Lightning Network (LN). Es importante que si comienzas desde cero conocimientos, comiences leyendo sobre qué es Lightning Network y cómo usarlo, conceptos básicos y funcionalidades. En esta guía encontrará muchos enlaces a esos recursos. NO los ignore, léalos y luego póngalos en práctica con su nodo Umbrel.

# Tabla de contenido
{: .no_toc .text-delta }
1. TOC
{:toc}

## **Enlaces importantes**
**Como nota general:** Si es nuevo en esta área de LN (Lightning Network), infórmese, al menos los conceptos básicos de LN, ANTES de iniciar el nodo Umbrel.

| Enlaces Principales | Enlaces para leer |
| ------ | ------ |
| [Página Info Umbrel](https://info.umbrel.tech/es/) (esta página)  | [Recursos Lightning Network - EN](https://www.lopp.net/lightning-information.html) – Jameson Lopp |
| [Página Get Umbrel](https://getumbrel.com/) | [Lightning Node Management](https://translate.google.com/translate?sl=auto&tl=es&u=https://openoms.gitbook.io/lightning-node-management/) – openoms |
| [Foro Comunidad Umbrel](https://community.getumbrel.com/) | [Guía de principiantes de LN](https://translate.google.com/translate?sl=auto&tl=es&u=https://bitcoiner.guide/lightning/) – Bitcoiner Guide |
| [Umbrel Community ES Telegram Group](https://t.me/umbrelspanish) | [LightningWiki](https://translate.google.com/translate?sl=auto&tl=es&u=https://lightningwiki.net/) – BitcoinStickers |
| [Umbrel Community Discord Server](https://discord.gg/VY3SsPZZya) | [Wiki Lightning Network](https://translate.google.com/translate?sl=auto&tl=es&u=https://wiki.ion.radar.tech/) – ION Radar Tech |
| [Página Umbrel Github](https://github.com/getumbrel/umbrel) | [Que es Lightning Network](https://academy.binance.com/es/articles/what-is-lightning-network) – Binance Academy |
|[Alex Bosworth sobre Routing Nodes](https://lightningjunkies.net/lnj036-alex-bosworth-talks-routing-nodes/) | [Descripción general implementaciones Lightning Network](https://translate.google.com/translate?sl=auto&tl=es&u=https://medium.com/@fulgur.ventures/an-overview-of-lightning-network-implementations-d670255a6cfa) |
| [Tutoriales vídeo sobre LN](https://www.youtube.com/c/MinistryofNodes/videos) – Ministry of nodes |	[Node Doctor](https://terminal.lightning.engineering) – Diagnostico sobre su nodo  |
| [Tutoriales vídeo sobre LN](https://www.youtube.com/user/renepickhardt/videos) – René Pickhardt | [ln.bigsun.xyz](https://ln.bigsun.xyz/) o [1ml.com](https://1ml.com/) o [Amboss](http://amboss.space) - Exploradores |
| [LN channels mangement](https://youtu.be/HlPIB6jt6ww) – Alex Bosworth | [Guia de Nodos LN en ES](https://estudiobitcoin.com/nodos-de-enrutado-en-la-lightning-network/) - Estudio Bitcoin |

## **Modos de instalación**

En primer lugar: la contraseña predeterminada del nodo Umbrel es **moneyprintergobrrr**

Segundo: CADA aplicación Umbrel tendrá una dirección onion diferente, por lo que si planea usarla fuera de su LAN, guarde esas direcciones onion, pero NUNCA las exponga / comparta en público.

Tercero: cambie la contraseña predeterminada de su nodo Umbrel, pero tenga en cuenta que, por el momento, la aplicación Thunderhub y Lightning Terminal no actualizará esa contraseña y seguirá teniendo la contraseña predeterminada. Puede cambiar esa contraseña, buscando manualmente en el vientre de cada repositorio de aplicaciones.

Cuarto: **ESPERE** a que el nodo se sincronice por completo y no envíe fondos al monedero de su nodo hasta entonces. No puedes usarlos de todos modos hasta que esté sincronizado, así que ¿por qué tanta prisa? **Paciencia** es la clave para un operador de nodo. 

### **Usando un Raspberry Pi 4**

- [Lista Hardware recomandado](https://community.getumbrel.com/t/recommend-hardware-links-for-different-countries/33) usando RaspPi para Umbrel node, recomendamos utilizar el hardware indicado. Hay muchos usuarios que han tenido problemas simplemente por usar otro hardware.
- [Video tutorial](https://youtu.be/fppmhqjqh2E) (por BTC Sessions) sobre instalar / configurar un nodo Umbrel desde cero, usando una Raspberry Pi
- [Github Umbrel](https://github.com/getumbrel/umbrel-os) instrucciones de instalación


### **Usando un normal PC/laptop/NUC**

- Mínimo recomendado. Configuración de hardware: CPU x64, memoria RAM de 8GB, HDD de 1TB (SSD óptimo). Un ejemplo aqui: [Gigabyte NUC](https://imgur.com/a/twdZANs)
- SO software base: [Debian](https://www.debian.org/) o [Ubuntu](https://ubuntu.com/download/desktop), O cualquier otra distribución de Linux con la que te sientas cómodo
- [Instrucciones de instalación en Github Umbrel para Linux](https://github.com/getumbrel/umbrel#-installation) – solo sigue las sencillas instrucciones y en 5 minutos lo tendrás instalado
- [Video tutorial para instalar Umbrel en Ubuntu](https://www.youtube.com/watch?v=Nh23rbT8yHA)

**Instrucciones paso a paso**
Abra la ventana de comandos de Terminal y primero actualice todo el sistema con:

`sudo apt-get update y sudo apt-get upgrade`

(reiniciar si es necesario)

1 - Instalar Docker

`sudo apt-get update`

luego

`sudo apt-get install docker-ce docker-ce-cli containerd.io`

2 - Instale Python, compruebe primero si hay una versión instalada con

`python3 –– version`

entonces, si no hay Python instalado o es demasiado antiguo, el siguiente paso

`sudo apt install python3.9.6`

3 - Instalar Docker Compose

`sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s) - $ (uname -m)" -o / usr / local / bin / docker-compose`

luego

`sudo chmod + x / usr / local / bin / docker-compose`

Si tiene problemas para instalar, consulte [instalación alternativa](https://docs.docker.com/compose/install/#alternative-install-options) o simplemente ejecute

`sudo pip install docker-compose`

4 - Instale requisitos adicionales

`sudo apt-get install fswatch jq rsync curl`

5 - Instalar Umbrel

Cree una carpeta para el repositorio de Umbrel:

`md Umbrel` y luego `cd Umbrel`

ejecuta el script de instalacion:

`curl -L https://github.com/getumbrel/umbrel/archive/v0.3.14.tar.gz | tar -xz --strip-components = 1`

HECHO

Iniciar Umbrel

`sudo ./scripts/start`

Como verá el mensaje al final del script de inicio, puede acceder a su nodo en http://umbrel.local o http://192.x.x.x (la IP local de su nodo). Simplemente use la misma máquina Linux para acceder a ella o cualquier otra PC desde su LAN para acceder al panel de la página de ese nodo. 


Una vez que está instalado y en ejecución, comienza a sincronizar Bitcoin Blockchain. En este momento, simplemente relájese y espere a que se sincronice. Tardará entre 4 y 12 días, según el hardware y la conexión a Internet. Mientras tanto, lo invito a aprender más sobre Lightning Network y cómo usar y estar preparado para usar un nodo Umbrel.

## **Reglas Generales**
### **Tamaño del canal**

Cuando abra canales, **NO** comience con una pequeña cantidad como 20-50-100k sats. Esa cantidad ridículamente baja no será suficiente ni siquiera para las tarifas de apertura / cierre. Los canales de poca cantidad le están haciendo más daño que bien a usted y al resto de la red .

Ejemplo: si tiene un canal de 20k abierto con mi nodo:

- que apenas cubre las tarifas de apertura / cierre y solo queda polvo para gastar.
- Si quiero usar ese canal para enviar 50k sats, no será posible. Por lo tanto, el canal será inútil y reducirá la puntuación del para ambos nodos.

Canales abiertos con min 500k-1M sats. Eso ofrecerá un mejor enrutamiento, para usted y para todos los demás que enrutarán txs a través de su nodo.

"Más grande es mejor" NO aplique demasiado en este caso, así que ahora no vaya al otro extremo y abra canales masivos de 0.5 BTC. Mucho mejor el enfoque de tener 5-6 canales salientes con cada uno entre 500k a 1M sats y también, dependiendo de sus necesidades, 3-4-5 canales entrantes, con la misma cantidad.

Ahora sí, más canales es mejor, porque tu nodo estará mejor conectado y encontrará una mejor ruta y más rápido.

**Liquidez de SALIDA**: tiene un nodo LN para realizar pagos LN, comprar cosas, enviar a amigos, pagar servicios, etc. Así que intente abrir canales LN con los comerciantes con los que está dispuesto a comerciar (comerciantes, intercambios, operadores de monederos, servicios LN, amigos con nodos).

**Liquidez ENTRANTE**: busque algunos pares que estén dispuestos a abrir algunos canales HACIA su nodo. Consulte el servidor Umbrel Discord y el grupo de Telegram, donde puede solicitar que sus pares abran canales con su nodo. La liquidez entrante ES NECESARIA para poder recibir pagos de LN .

**¿Qué / con quién debo abrir canales?**
- Primero, con esos comerciantes / tiendas que harás txs, les comprarás, los txs tendrán tarifas cero y tienes enrutamiento directo.
- En segundo lugar, con amigos / fanáticos de LN que conoce y puede crear un anillo de nodos con una cantidad específica de sats para canales, equilibrando los fondos y reduciendo las tarifas entre los nodos en el anillo.
- En tercer lugar, su anillo de nodos puede tener algunas conexiones / canales "externos" con otros buenos nodos (lo ideal es que cada miembro del anillo tenga un nodo diferente) para que pueda enrutar fácil y rápidamente a cualquier lugar.

### **Operador de nodo**

Operar un nodo no significa automáticamente "Me haré rico ganando sats". Aléjate de esta mentalidad equivocada.

A. Mantener el nodo Umbrel para proteger su privacidad, para proteger sus llaves, para proteger su dinero, para proteger la custodia de sus billeteras.

B. Sí, ganarás algunos sats, pero solo si administras tu nodo de la manera correcta. Pero incluso entonces, esas ganancias serán insignificantes para ser consideradas "ganancias".

C. Mantener este nodo para APRENDER más sobre Bitcoin y encontrar soluciones para aplicaciones de la vida real. Umbrel ofrece esa oportunidad con muchas aplicaciones incluidas (consulte LNbits, BTCPay, LN Pool, Whirlpool) listas para casos de uso.

D. NO APAGUES EL NODO! He visto gente que apagan su nodo por la noche (que no pueden dormir). Para que lo has puesto en march si no lo tienes encendido 24/7/36? Un nodo tiene que estar encendido el maximo tiempo posible. Ademas es recomendable ponerle un SAI y evitar las caidas de corriente electrico. Este es su Banco y tambien otros dependen de esta conexion continua.

## **Uso basico del nodo**

Ahora, lo que sugiero, para cualquier nuevo usuario de nodo, en especial con Umbrel:

### **Espere sincronizar la cadena de bloques**

Tomará tiempo, lo sé, pero tenga paciencia. Pasará, no se preocupe. No hagas nada (estúpido) hasta que no esté listo. Dependiendo de la configuración de su hardware y la velocidad de Internet, podría demorar entre 3 y 12 días. Después de la sincronización, se necesitarán otras 8-12 horas para sincronizar el índice de Electrum Server. 

### **Mantenga su software actualizado**

Entonces, una vez sincronizado, verifique si hay nuevas actualizaciones y aplíquelas. Si está utilizando la opción RaspPi, la actualización de Umbrel también actualizará el sistema operativo. Si está utilizando un Linux con Umbrel en Docker, le recomendaría realizar el siguiente procedimiento (en la Terminal):
- Detener el nodo Umbrel: `sudo ./scripts/stop`
- Guarde su archivo lnd.conf si lo editó (agregue color, nombre o características específicas a su nodo), vea en home/umbrel/lnd
- Sistema de actualización: `sudo apt update` y luego `sudo apt upgrade`
- Reiniciar el sistema
- Actualizar Umbrel: `cd ~/umbrel && sudo ./scripts/update/update --repo getumbrel/umbrel#v0.3.9` (reemplace la versión con la última versión)
- Reemplace el archivo lnd.conf con el que lo guardó antes de actualizar
- Iniciar Umbrel: `sudo ./scripts/start`
- Deje que el nodo se ponga al día con los bloques y los registros, suele tardar varios minutos, tenga paciencia. Ahora puede ingresar al dashboard Umbrel

### **Instale las aplicaciones mínimas necesarias**

Consulte la sección Aplicaciones de Umbrel:
- Ride The Lightning (RTL) – management de nodo, wallet, canales, routing.
- ThunderHub – management de nodo, wallet, canales, routing, chat, tools.
- BTC RPC Explorer – Blockchain explorer y herramientas
- Mempool – ver tarifas de mempool, bloques, verificar txs, explorador de blockchain, muy útil

### **Instale aplicaciones opcionales** 

Consulte la sección Umbrel de App Store, instalación con un clic:

- [LNBits Suite](https://info.umbrel.tech/guides.html#umbrel--lnbits) – increíble conjunto de herramientas con LNURLpay/retire, LN TPoS, LNDHUB y muchos más.
- [LNDHub BlueWallet](https://info.umbrel.tech/es/guides.html#umbrel--lndhub--bluewallet) - conviértase en el banco de su familia y amigos que no tienen un nodo. Manera asombrosa de tener la custodia propia de sus fondos con un nodo.
- [BTCPay Server](https://btcpayserver.org/) – muy buen backend para tiendas web/tiendas, donaciones, POS, herramienta comercial, [YT channel aqui](https://www.youtube.com/c/BTCPayServer/featured)
- Samourai Whirlpool – herramienta de privacidad y mixing, conexión para su monedero Samourai. [Una muy buena guía detallada aquí](https://bitcoiner.guide/) y una entrevista con el [desarrollador de la billetera Samourai aquí](https://stephanlivera.com/episode/150/)
- [Sphinx Chat](https://sphinx.chat/) – herramienta muy interesante para podcasters, chat de grupos privados usando LN para consejos, propinas y chats. ¡Esté atento! Para que NO se le cobre una tarifa CADA vez que inicie sesión o cambie de perfil, tendrá que [abrir un canal de 100k sats con el nodo de la aplicación Sphinx ](https://github.com/stakwork/sphinx-relay/wiki/Home-node-FAQ)

### **Conecte algunos monederos a su nodo** 

A. Consulte "Conectar monedero" en la página Dashboard Umbrel que tiene instrucciones fáciles de entender para conectar muchos monederos:
- Zap, Zeus: para administrar su nodo de forma remota, utilizando una conexión onion (versión de escritorio y móvil)
- Electrum: para verificar/transmitir sus txs desde su monedero usando su propio nodo, también usándolo como fondos de intercambio desde/hacia su nodo
- Spectre: conéctese a su nodo y utilice sus hardware wallets favoritos de forma segura
- Y muchos más... 

B. [Bluewallet + LNDHub BW](https://info.umbrel.tech/es/guides.html#umbrel--lndhub--bluewallet): para ofrecer monederos alojados en su nodo a su familia y amigos. ¡Esta es una gestión de monederos, no una gestión de nodos! 

C. [BlueWallet / Zap + LNbits](https://info.umbrel.tech/es/guides.html#umbrel--lnbits): una buena manera de tener sus propios monederos y extensiones lndhub para diferentes usos. 

### **Conecte su monedero de nodo onchain con [Bluewallet](https://bluewallet.io/)**

Sí, es cierto, ahora puede conectarse directamente a una aplicación móvil, el monedero AEZEED en cadena. Simplemente use su semilla de nodo Umbrel en Bluewallet (agregándola como nuevo monedero) y listo. Casos de uso: 

- desea tener a mano una forma de depositar rápidamente en su billetera de nodo (onchain)
- necesita tener acceso a sus fondos en la cadena en caso de que su nodo se bloquee y desee recuperar/acceder a los fondos

## **Como financiar su nodo**

- Tenga en cuenta: su monedero Umbrel onchain debe actuar como un puente / rampa entre sus muchas otros monederos o fuentes BTC y sus canales LN. Este monedero se utiliza para financiar los canales de apertura y cierre en su nodo LN. Entonces, para poder abrir canales LN, deberá enviar algunos fondos a esta billetera en cadena. 
- Vaya a la interfaz principal de Umbrel, monedero Bitcoin y haga clic en "recibir". Desde cualquier otro monedero envíe algunos fondos (generalmente para 3-4 canales, cada 1M sats). Espere a que se confirme y luego vaya al paso siguiente.

- Puede usar la billetera Lightning, desde la misma interfaz principal de Umbrel, pero sugiero usar la aplicación Thunderhub. ¿Por qué? Porque puedes controlar mejor las tarifas de los mineros por abrir el canal. Además, la aplicación RTL es buena, pero aún así no puede ver la tarifa en total que está pagando. Así que use una de estas 3 interfaces y vaya a canales abiertos. Seleccione un nodo deseado y coloque al menos 500k-1M sats y abra un canal. Comience con un buen nodo (consulte [https://ln.bigsun.xyz](https://ln.bigsun.xyz) o [https://1ml.com](https://1ml.com)) qué nodo tiene el historial más largo o el número de canales) y luego continúe con los nodos de otros usuarios. Ayude a los nuevos usuarios que publiquen su URI de nuevos nodos en el grupo de Telegram de la comunidad Umbrel y solicite nuevos canales. JUNTOS MÁS FUERTES.

- Una vez que abres un canal LN, hasta que no se confirme (normalmente toma 3 confirmaciones) la UTXO desde donde usaste los sats para abrir el canal no será utilizable hasta que no se confirme la nueva UTXO. [Vea aquí lo que significa UTXO](https://learnmeabitcoin.com/technical/utxo). En otras palabras, hasta que no se confirme, no puede usar los sats restantes para abrir un nuevo canal. Esperar. Sea paciente, no se asuste porque sus fondos no se reflejan en el saldo total (todavía). Opcional, puede ir a la aplicación RTL y verificar los UTXO en el monedero onchain. 

- Equilibre sus canales LN. Puede usar la aplicación Umbrel Thunderhub para abrir y equilibrar sus canales con los de sus pares. Esto proporcionará un mejor enrutamiento para sus txs y también para otros usuarios txs que están enrutando a través de su nodo. [Aquí tienes una guía sencilla sobre el uso de Thunderhub](https://translate.google.com/translate?sl=auto&tl=es&u=https://apotdevin.com/blog/thunderhub-balancing). Otra opción podría ser: abrir un canal hacia un par, digamos 1M sats. Luego, una vez que esté abierto, vaya y realice un pago a un monedero externo (por ejemplo, Bluewallet) con la mitad del saldo. De esta manera está equilibrando el canal para txs de entrada y salida. Opcionalmente, puede sacar del mismo canal (loop out), volver a su monedero de nodo onchain y reutilizar esa cantidad para abrir otro canal con otro par. 

- ¿Vas a hacer algunas compras con LN? Bien, entonces vaya y abra canales con los comerciantes a los que planea comprar. De esta manera, puedes pagar menos tarifas, tus tx pasarán más rápido, también ayudas a los comerciantes a tener una mejor entrada. Opcional, puede pedirles que reduzcan las tarifas de su canal. Incluso si no planeas hacer esas compras con ellos, no importa, solo abre los canales. Se utilizará también para enrutamiento, si hay una buena cantidad.

- En [servidor Umbrel Discord](https://discord.gg/VY3SsPZZya) también puedes encontrar una sala donde los nuevos usuarios de Umbrel pueden intercambiar información para abrir canales mutuos. Eso ayudará mucho a los nuevos usuarios, para empezar con algunos operadores de nodos conocidos y con algo de liquidez.

- **Tutoriales en video sobre cómo abrir/financiar canales LN:** 

    - [Video tutorial como hacer un nodo Umbrel](https://youtu.be/fppmhqjqh2E) – por BTC Sessions
    - [Video tutorial como utilizar Umbrel + Thunderhub](https://www.youtube.com/watch?v=KItleddMYFU) - por BTC Sessions
    - [Video Tutorial RTL app](https://youtu.be/DfRYJcBsfkA?t=155) – por Ministry of Nodes
    - [Video Turorial RTL rebalancing channels](https://www.youtube.com/watch?v=09tLEZM4MgQ)
    - [Video Tutorial ThunderHub app](https://www.youtube.com/watch?v=hZ3ttYFliL4) – por ThunderHub
    - [LN channels mangement](https://youtu.be/HlPIB6jt6ww) – por Alex Bosworth

- **Anillo de fuego de Lightning Network** - Interesante grupo para crear un anillo de nodos, abriendo canales entre sí. [Aquí hay una mejor explicación de cómo funciona esto](https://github.com/Czino/ring-of-fire). Si desea unirse a su grupo de Telegram y participar en diferentes tipos de anillos, [aquí está el enlace](https://t.me/joinchat/Uao0Z_hBequXkeB0)

- Un muy buen hilo para leer, sobre [como re-abalncear los canales LN aqui](https://community.getumbrel.com/t/to-read-do-all-lightning-channels-need-to-be-balanced/817), por Czino (Rings of Fire)

- Espera a que se confirme el canal de apertura (generalmente 3 confirmaciones) y listo, estás listo para comenzar a relámpago, usando las billeteras que indiqué antes.


## **Caracteristicas necesarias en Umbrel**

* Me gustaría ver futuros lanzamientos de Umbrel que puedan abrir canales usando todo el saldo de la billetera en cadena, deduciendo automáticamente la tarifa. Adivinar la tarifa correcta siempre termina con algunos depósitos de polvo que quedan en la billetera de la cadena. Eso no es bueno.
* Servicios de transmisión. Sería bueno tener un servicio de transmisión, como una aplicación separada. ¿Quizás como característica pro / paga, para que los desarrolladores puedan recibir algo?
* Aplicación BTCPay: opción para apagar Tor o agregar la opción para usar Tor / clearnet para una tienda web externa. En este momento, la instancia de BTCPay de Umbrel se puede usar ÚNICAMENTE con las tiendas web .onion. No esperamos que las niñas compren cosméticos y zapatos con Tor ...

## **Para concluir**

Espero que esta mini-guía te ayude como novato a comenzar a usar tu nodo. Edúquese lentamente y encuentre la mejor manera de usar su nodo.

Hay muchas otras cosas que hacer con un nodo, también muchas otras aplicaciones y usos, pero esto es solo el comienzo.


**HAPPY LIGHTNING!**

---

Esta guía fue creada por [DarthCoin](https://bitcoinhackers.org/@DarthCoin). Puedes probar su nuevo nodo Umbrel con una propina para DarthCoin, generarando un LNUrl [aquí](https://lntxbot.bigsun.xyz/@DarthCoin). El LNUrl se puede leer con Bluewallet, Zeus, Zap o la aplicación Thunderhub dentro de Umbrel. 
