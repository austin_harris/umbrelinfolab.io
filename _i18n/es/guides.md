Le vamos a presentar una serie de mini guias, sobre apps que pueden utilizar con el nodo Umbrel. Intentaremos actualizar esta página para mantenernos al día con las nuevas aplicaciones y funciones, así que estad atentos, agregue esta página a favoritos para más adelante.

Tabla de contenido
{: .no_toc .text-delta }
1. TOC
{:toc}

## **Umbrel + LNDHub + Bluewallet**

### **Descripción**

Este es un gran paso adelante para todos los operadores de nodos. **¿Por qué?** Porque es un paso más para que un nodo se convierta en un "Banco soberano de Bitcoin", ser un HUB para su familia, amigos y todos aquellos que no tienen / no pueden tener un nodo para sí mismos. 

Más detalles sobre [BlueWallet LNDHub aquí](https://bluewallet.io/lndhub/) y también aquí está [el anuncio del equipo de Bluewallet](https://bluewallet.io/BlueWallet-brings-zero-configuration-Lightning-payments-to-iOS-and-Android-30137a69f071/), en ingles.

Prácticamente puedes “alquilarles” espacio y liquidez desde tu proprio nodo. Hay un aspecto muy importante en el mundo de Bitcoin y pocos lo entienden: 
- monederos de custodia (sus fondos están bajo la custodia y el control de otras partes) 
- monederos privadas (sus fondos están bajo su propia custodia y usted es el único que los controla) 

**NOTA - Por el momento, la aplicación móvil BW NO administrará su nodo / canales como lo hacen las apps Zeus o Zap.**

Ahora mismo está activada solo la parte de LNDHub de su nodo. Entonces, si activa e instala esta aplicación ahora solo para ver sus canales, **no espere verlos.** 

**Ya puedes ser un "banquero" para tu familia y amigos!**

### **Conceptos erróneos**

- Bluewallet LNDHub es un administrador de billetera y NO un administrador de nodos. Es una gran diferencia.
- Con tu nodo y aplicaciones como Zeus, Zap, RTL, Thunderhub, estás administrando “el banco”, tu nodo, tus canales, tú eres el banquero, moviendo fondos, ofreciendo liquidez para tus propios usuarios internos.
- Con BW LNDHub, gestiona los "clientes" de su banco, su acceso a la liquidez de su banco y gestiona sus propias "cajas de seguridad" en su nodo.
- Piense en que un monedero LNDHub es como un vaso vacío. Su nodo LNDhub es el proveedor para transportar el agua hacia / desde su vaso de agua. Obtienes agua en este vaso de otras fuentes (NO de tu nodo LN) y tu nodo la transporta (liquidez). 

Pero tenga en cuenta que la custodia propia también significa que tendrá: 
- más responsabilidad por sus propios fondos, para mantener seguros y privados sus fondos y monederos
- más conocimiento, aprendiendo a usar estas nuevas herramientas
- más costos (a veces, si no sabe cómo hacerlo correctamente), los errores cuestan, por lo que es mejor que aprenda primero a hacerlo correctamente
- más responsabilidad para aquellos que usarán su nodo / monederos / fondos 
- ofrecer liquidez, esto también puede resultar en más costos 

Entonces, si hasta ahora, estaba usando BlueWallet (BW), estaba usando un monedero LN de custodia (no onchain). Eso significa que los servidores de BW estaban a cargo de administrar su monedero LN, crearla y ofrecer liquidez para poder usar sats en LN sin molestarse en ejecutar un nodo, mantenerlo, abrir canales, mantener la liquidez alta.

### **Uso de LNDHub**

- conecte su aplicación móvil BW a su nodo 
- crea un monedero LN en tu aplicación movile BW 
- ese monedero LN tendrá saldo 0 (cero), pero podrá usar la liquidez de sus canales, NO el saldo (!) 
- cree tantas billeteras como desee para su familia y amigos, ellos administrarán sus propios monederos y saldos, pero tenga en cuenta: usted es responsable de que sus billeteras estén en línea y puedan realizar transacciones.
- Las txs entre estos monederos lndhub serán con tarifa cero, no atravesará toda la red de nodos, seria como una tercera capa, solo ocurrirá dentro de su nodo
- un escenario interesante podría ser el de un restaurante que ofrece a sus clientes la opción de tener sus propias “monederos lndhub del restaurante” y pagar con tarifa cero, instantáneo y privado. Estos monederos se pueden recargar desde fuentes externas, desde sus propios monederos onchain y pueden pagar donde quieran. Cuando paguen al restaurante, pagar a otro monedero lndhub desde el mismo nodo será cero fee.
- Puede financiar esta nuevo monedero vacío de diferentes fuentes:
   - monedero BW onchain: en el monedero, haga clic en "administrar fondos" y seleccione la opción "recargar". Aparecerá uno de sus monederos onchain que ha configurado en su aplicación BW. Si tienes uno. Se podría importar (usando la semilla del nodo) el monedero onchain desde su nodo, pero eso no se recomienda. Será mejor que uses uno separado. Opcionalmente, puede usar un monedero "externo" (en la misma pantalla emergente) y generará una factura onchain que se pagará desde otro monedero onchain. Esta tx se trasladará más tarde a LN usando algo de magia de LN :)
   - cualquier otro monedero externo de LN (¡no desde su nodo!), simplemente haciendo una factura para pagarla desde su nuevo monedero lndhub 

### **Cómo conectar BW a su nodo**

a. Actualice la aplicación móvil Bluewallet: 
- En Android 9+, actualice a la versión 6.1.1. Puede usar su [Play Store](https://play.google.com/store/apps/details?id=io.bluewallet.bluewallet) (si aún lo tiene) o [BW Github source](https://github.com/BlueWallet/BlueWallet/releases) directamente. 
- En Android 8 o inferior, quédese con la v.6.0.8 hasta nuevas actualizaciones. La version 6.1.1 esta restringida en versiones de Android inferiores de 8. Puedes seguir utilizando la v 6.0.8 con Orbot (Tor) si quieres, funciona bien. Puedo decir que aun mejor. La v 6.1.1 en Android 9+ a veces se bloquea por culpa de Tor.
O si puede actualizar la versión de su sistema operativo, será bueno. Si no puede, entonces es hora de que compre un nuevo teléfono serio. Recomiendo un Pixel y luego instalar un sistema operativo nuevo limpio como GrapheneOS o CalyxOS en él. 
- En iOS, vaya a la [App Store](https://itunes.apple.com/app/bluewallet-bitcoin-wallet/id1376878040) y realice la actualización. 

b. Actualice su nodo Umbrel a la versión 0.3.10 (ya sabe cómo) 

c. Instale la aplicación BlueWallet LNDhub en Umbrel. Vaya a App Store e instálelo.

d. Conecte su aplicación móvil BW con su nodo. Abra la sección "Conectar billetera" y abra las instrucciones de Bluewallet. Abra su aplicación móvil BW y siga esas instrucciones, paso a paso:
- en BW, entra en Opciones - Red - activar Tor. Hacer un test. Reiniciar BW (indicado con force close)
- en BW, entra en Opciones - Red - Electrum Server. Escanear el codigo de la direccion de tu Electrum Server (onion) y el puerto 50001. Hacer una prueba. Reiniciar BW
- al abrir BW, si ya tienes algun mondereo creado, esperar si se actualice, puede tardar un tiempo. si la app se bloquea, volver a reiniciar. A veces el Tor hace cosas raras en Android 9+ con v 6.1.1
- entra en Opciones - Red - Lightning. Ahora ya puedes escanear el QR de su nodo LNDhub.
- volver a la pantalla principal y añadir un nuevo monedero LN. Solo le das crear y listo, ya tiene un monedero limpio conectado a su nodo. Este nodo no tiene fondos, vas a tener que alimentarlo desde un otro monedero LN. Este monedero SOLO utiliza la liquidez de su nodo para poder enviar atraves de LN.

### **Probando los nuevos monederos**
Pago mediante una billetera de nodo externo:
- Abra ese nuevo monedero, cree una factura de 100 sats, copie 
- En la misma aplicación móvil de BW, vaya a su billetera BW LN habitual y haga clic en enviar, pegue la factura que creó desde su billetera LNDhub y envíela. (esto no me funcionó, dice "pago en tránsito", es decir, no puedo encontrar la ruta correcta entre los servidores de BW y el mío). Ok, esto puede suceder. 
- Creé una nueva factura de 120 sats en mi billetera LNDhub y fui a [Telegram LNtxbot](https://telegra.ph/LNtxBot-Guide-01-24) y la pagué. Tambien si tienes el monedero LNtxBot importado en tu BW, lo puedes hacer desde alli mismo. Pasó perfectamente. 
- Ir en Umbrel a RTL - LN - Transacciones y en las facturas puedo ver los 120 sats recibidos y también aparecer en el saldo de mi billetera LNDhub 
- Intento de pago al reverse, desde monedero LNDhub, al monedero LNtxBot. Así que fui a mi [LNtxbot LNurl](https://lntxbot.bigsun.xyz/@DarthCoin) y pagué 100 sats + 1 sat fee con la billetera LNDhub de mi Umbrel 
- La Tx apareció en transacciones RTL inmediatamente 

Pago usando 2 billeteras LNDhub, en el mismo servidor LNDhub (su nodo): 
- cree dos monederos lndhub (podrían estar en la misma aplicación BW), pero para mayor comodidad use dos moviles con BW conectado a su LNDhub
- financiar uno de ellos (monedero A) desde un monedero externo (ver escenario anterior)
- en el monedero B, cree una factura de 100 sat.
- abra el monedero A, escanee esa factura y pague
- ¡Eso es! La TX pasó por instantáneo, con tarifa CERO, privado completo
- por el momento, la tx aparecerá en sus aplicaciones LND (RTL / TH / Umbrel) como facturas “impagas”, pero en realidad se pagan. Quizás en el futuro tengamos una forma de tratar estos tx como "tx internos" y no como "facturas".

**YA ESTA TODO! FUNCIONA BIEN. AUN FALTA MAS COSAS POR VENIR...**

NOTA: Si tiene problemas o desea agregar algo con respecto a esta guía, puede visitar el [Foro de la comunidad Umbrel](https://community.getumbrel.com/) (ingles) donde puede encontrar aún más guías, procedimientos, discusiones y muchas personas que pueden ayudar. 

## **Umbrel + LNBits**

### **Descripción**

LNBits es una aplicación increíble que agrega un conjunto de funciones al nodo Umbrel y los usuarios pueden adaptarse a sus necesidades. Esta aplicación utiliza las liquidez del nodo (onchain y LN), y agrega una base de datos separada para la administración. Puede usarlo para múltiples monederos, como un LNDhub o un uso específico para comerciantes, creadores de contenido, contenido web, cajeros automáticos LN y muchos otros.

Enlaces de LNBits:
- [LNBits.com](https://lnbits.com/) - instancia de prueba pública
- [LNBits.org](https://lnbits.org/) - Más información y fuente de descarga
- [Página de Github](https://github.com/lnbits/lnbits) - participar en el código fuente abierto con detectar bugs o propuestas
- [Canal Youtube LNBits](https://www.youtube.com/channel/UCGXU2Ae5x5K-5aKdmKqoLYg/videos) - con demostraciones y tutoriales
- [LNBits Telegram Group](https://t.me/lnbits) - donde puede obtener más ayuda directamente de los desarrolladores

NOTAS:
- Esta aplicación Umbrel funciona solo detrás de Tor, por lo que cualquier extensión que agregue / use / cree QR, deberá usarla desde la dirección .onion proporcionada. ¡No compartas esta dirección onion!
- Si desea usarlo en clearnet, [aquí hay una guía increíble para seguir](https://community.getumbrel.com/t/guide-lnbits-without-tor/604).
- Una vez que abra / cree un nuevo monedero LNBits, **guarde ese enlace de dirección en sus marcadores**. NO hay otra forma (por el momento) de recuperar este monedero o iniciar sesión nuevamente. La dirección contiene la clave de su monedero. ¡No comparta esta dirección!
- LNBits aún está en fase beta, así que téngalo en cuenta y no lo use en un escenario de "producción".

### **Funcionalidades - extensiones**

- [LNurl-pagar](https://www.youtube.com/watch?v=WZpK4xfGcuY) - una forma sencilla de tener un código QR LN estático que no expira para recibir
- [LNurl-retirar](https://www.youtube.com/watch?v=TUmsHpJtveQ) - una buena manera de crear cupones cargados con sats y otros pueden simplemente escanear y retirar en su monedero LN
- [TPoS](https://www.youtube.com/watch?v=6yNBbDWf-RE) - LN TPV virtual para pequeñas tiendas
- [LNDHub](https://www.youtube.com/watch?v=WWrAayAfqaQ) - cree varios monederos LND para familiares, empleados y clientes con diferentes niveles de acceso.
- [DJ Livestream](https://www.youtube.com/watch?v=zDrSWShKz7k) - crea tu propia máquina de tracks de musica pagada con sats
- [SatsPayServer](https://www.youtube.com/watch?v=p0QZe9yO64Y) - extensión para crear cargos onchain y LN
- [Tienda offline](https://www.youtube.com/watch?v=_XAvM_LNsoo) - reciba pagos por productos fuera de línea, solo escaneando codigo QR
- Eventos - venda y registro de entradas para eventos
- Captcha - crea captcha para detener el spam, pagado con sats
- [Paywall](https://www.youtube.com/watch?v=N0fU_0gbaXs) - cree paywalls para el contenido web
- Subdominios - vende subdominios de tu dominio
- [Tickets de soporte](https://www.youtube.com/watch?v=iq6BLfLsU6Y) - sistema de tickets de soporte pagados por LN
- [Watch-only](https://www.youtube.com/watch?v=Xx09tzeZIok) - agregue sus monederos offline en el modo "solo vista" con total privacidad y seguridad
- [Administrador de usuarios](https://www.youtube.com/watch?v=E-IjtCHUVBE) - genera usuarios y monederos y administrarlos
- Bleskomat - conecte un cajero automático Bleskomat (cajero LN) a un servidor LNBits, vendes sats por moneditas
   - [Tutorial de cómo construir un cajero automático FOSSA LN](https://www.youtube.com/watch?v=1FeE74sDNGA)
   - [DIY Construye tu proprio cajero Bleskomat LN](https://github.com/samotari/bleskomat) y aqui un [ejemplo de uso](https://degreesofzero.com/article/beyond-coffee-bitcoin-for-every-day-use-with-lnurl.html)
   - [Demostración de cómo usar un cajero automático LN](https://www.youtube.com/watch?v=DGFVhgHwt3E)
   - Aquí hay [una documentación de código abierto](https://docs.lightningatm.me/) sobre cómo construir su propio cajero automático LN y luego conectarlo a su servidor LNBits para su administración. 

¡Y muchas más extensiones por venir!

### **Como podemos utilizar LNbits**

Este es solo un ejemplo practico de como conectar el monedero LNBits. Consulte el [Canal Youtube LNBits](https://www.youtube.com/channel/UCGXU2Ae5x5K-5aKdmKqoLYg/videos) para ver mas ejemplos.
- Instala en Umbrel la aplicación LNBits. Esto activará la opción para crear varios monederos LN en su nodo, que se pueden vincular mediante la conexión lndhub.
- Cree un monedero en su módulo LNbits, active la extensión LNDHUB y vincúlela a ese monedero LNbits. Utilice la dirección onion para este procedimiento, NO la IP local o umbrel.local. El monedero debe ser visible a través de Internet y la única forma es a través de Tor (onion).
- Guarde en sus marcadores esa dirección de monedero, porque es la única forma de abrirla nuevamente, el enlace contiene la clave del monedero. Las nuevas versiones de LNbits tendrán una página de inicio de sesión / autenticación, pero por el momento guarde ese enlace de monedero. 
- Abra su Bluewallet, vaya a agregar monedero, haga clic en importar y escanee el código QR que se muestra en su LNDHUB de LNbits. Tiene dos opciones: como administrador (derechos completos sobre el monedero) o solo facturación (derechos limitados solo para crear facturas).
- **RECUERDE**: use Bluewallet con el servicio Tor (aplicación Android Orbot); de lo contrario, no leerá ese código QR.
- Hecho. Ahora puede usar esa billetera como una billetera LN normal en Bluewallet, sin necesidad de abrir canales, etc., todo está vinculado con su nodo LN, por lo que depende de la liquidez de su nodo. Esta es una buena opción si tiene una tienda pequeña con empleados que pueden cobrar usando LN, sin tener permisos completos para su nodo.

Esta publicación se actualizará con más contenido, en función de nuevas funciones o se requiere información adicional.

NOTA: Si tiene problemas o desea agregar algo con respecto a esta guía, es bienvenido al [Foro de la comunidad Umbrel](https://community.getumbrel.com/), donde puede encontrar aún más guías, procedimientos, discusiones y muchas personas que pueden ayudar. 

---

Esta guía fue creada por [DarthCoin](https://bitcoinhackers.org/@DarthCoin). Puedes probar su nuevo monedero LNDHub BW Umbrel con una propina para DarthCoin, generarando un LNUrl [aquí](https://lntxbot.bigsun.xyz/@DarthCoin). El LNUrl se puede leer con BlueWallet, Zeus o Zap wallet o la aplicación Thunderhub dentro de Umbrel. 
