## Recursos adicionales de Umbrel
{: .no_toc }

Esta página presenta información adicional acerca de Umbrel que no puede ser encontrada en su página oficial.
La intención de esta página es responder a preguntas generales acerca de Umbrel.

---

 Esta no es una página oficial de Umbrel.
 Esta guía sólo aplica para la última versión de Umbrel (actualmente v0.3.9).

---

## Tabla de contenido
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Acerca de esta guía

### Estructura

1. Introducción (esta página)
2. [Empezando con Umbrel](getting-started.html): Pasos iniciales con el nodo Umbrel.
3. [Guias applicaciones Umbrel](guides.html): Que podemos hacer con las apps de Umbrel
4. [Solución de Problemas](troubleshooting.html): Problemas acerca las funciones principales de tu nodo Umbrel.
5. [Preguntas Frecuentes](faq.html): Preguntas generales e información acerca de la tecnología usada en Umbrel.

---

## Una pequeña advertencia

Umbrel es un proyecto aún en fase Beta. Con lo cual no recomendamos tener más dinero en tu nodo Umbrel del que estés dispuesto a perder.

---

Esta página es parte de [Umbrel Labs](https://umbrel.tech), un servicio gratuito para expandir tu nodo Umbrel.

---
